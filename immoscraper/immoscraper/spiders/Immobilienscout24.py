# -*- coding: utf-8 -*-
import scrapy
import json
import urllib.parse


class Immobilienscout24(scrapy.Spider):

    name = "immobilienscout24"

    # settings for JSON-Request
    custom_settings = {'JOBDIR': "immobilienscout24",
                       'USER_AGENT': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'
                    }

    header_json = {
            'accept': 'application/json; charset=utf-8',
            'accept-language': 'de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7',
            'content-type': 'application/json; charset=utf-8',
            'x-requested-with': 'XMLHttpRequest'
    }

    # immobilienscout24 search endpoint url
    search_url = 'https://www.immobilienscout24.de/Suche'

    def start_requests(self):
        """
        Create initial requests through hidden search api of i24
        :return: Initial requests
        """

        # set pagenumber
        pagenumber = 1

        # imobilienscout24 search endpoint
        params = {
            'country': 'de',
            'city': 'berlin',
            'county': 'berlin',
            'search': 'wohnung-mieten'
        }
        # immobilienscout24 search parameters
        additional_params = {
            'price': '-700',
            'livingspace': '50.0',
            'sorting': '2',
        }

        # make sure pagenumber is sanitized
        if pagenumber >= 1:
            additional_params['pagenumber'] = pagenumber

        # attach search parameters to url if there exist
        additional_params_url = "?{}".format(urllib.parse.urlencode(additional_params)) if bool(additional_params) else ""

        # retrieve search results considering given page and the search parameters
        yield scrapy.Request(url="{}/{}/{}/{}/{}{}".format(self.search_url, params['country'], params['city'], params['county'], params['search'], additional_params_url),
                             method='POST',
                             callback=self.parse_search_result,
                             headers=self.header_json,
                             meta={
                                 'params': params,
                                 'additional_params': additional_params,
                                 'dont_cache': True
                                 # don't cache, to get current results
                             },
                             dont_filter=True)  # don't filter to retrieve current results

    def parse_search_result(self, response):
        """
        Processes search results, retrieve exposes and retrieve further search results
        :param response: response from previous request
        :return: further requests
        """
        # parse json response as python-dict
        jsonresponse = json.loads(response.body_as_unicode())

        # select only search results
        items = jsonresponse['searchResponseModel']['resultlist.resultlist']['resultlistEntries'][0]['resultlistEntry']

        # select maximum of search_pages
        max_pages = jsonresponse['searchResponseModel']['resultlist.resultlist']['paging']['numberOfPages']

        # visit each search result separately
        for item in items:
            id = item["@id"]
            metadata = {'id': item["@id"],
                        'publishDate': item["@publishDate"],
                        'creation': item["@creation"],
                        'modification': item["@modification"]
            }
            # retrieve expose for each id in search result
            yield scrapy.Request(url="https://www.immobilienscout24.de/expose/{}".format(id),
                                 callback=self.parse_expose,
                                 meta={
                                     'dont_cache': False,  # cache result
                                     'metadata': metadata
                                 })  # don't visit elements twice

        # search next page if exists
        if response.meta["additional_params"]["pagenumber"] < max_pages:
            params = response.meta["params"]
            response.meta["additional_params"]["pagenumber"] += 1
            additional_params_url = "?{}".format(urllib.parse.urlencode(response.meta["additional_params"]))
            yield scrapy.Request(url="{}/{}/{}/{}/{}{}".format(self.search_url, params['country'], params['city'], params['county'], params['search'], additional_params_url),
                                 method='POST',
                                 headers=self.header_json,
                                 callback=self.parse_search_result,
                                 meta={
                                     'params': response.meta["params"],
                                     'additional_params': response.meta["additional_params"],
                                     'dont_cache': True
                                     # don't cache, to get current results
                                 },
                                 dont_filter=True)

    def parse_expose(self, response):
        # get content javascript tags
        for script_element in response.xpath('//script/text()').getall():
            # split content with 'keyValues = '-string
            splitted_script_element = script_element.split("keyValues = ")
            # if 'keyValues = '-string exists
            if len(splitted_script_element) > 1:
                # then convert javascript string to dict
                expose = json.loads(splitted_script_element[1].split("}")[0] + str("}"))
                # add the metadata to expose-data
                for key_name in response.meta["metadata"]:
                    expose[key_name] = response.meta["metadata"][key_name]
                expose['id'] = int(expose['id'])
                # and return expose
                yield expose