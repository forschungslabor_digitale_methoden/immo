# -*- coding: utf-8 -*-
import os, json
import pandas as pd


class StoragePipeline(object):
    # Note: It is better to use a database for this task or do the drop_duplicate once after the scraper finished,
    # but this is a simple work-of-concept
    def process_item(self, item, spider):

        df_new_item = pd.DataFrame.from_records([item], index='id')

        if os.path.isfile('data.csv'):
            df_existing = pd.read_csv('data.csv', index_col=0)
        else:
            df_new_item.to_csv('data.csv')
            return item

        try:
            df_new = pd.concat([df_existing, df_new_item], join='inner', verify_integrity=True)
            df_new.to_csv('data.csv')
        except ValueError:
            print("Item already there!")
        return item
