# Quick start

1) Copy or clone the source locally: ```git clone https://gitlab.rrz.uni-hamburg.de/forschungslabor_digitale_methoden/immo.git```
2) Change to directory.
3) Install requirements via running ```pip install -r requirements.txt``` (either globally or in a py-env) from the source folder.
4) Run ```python main.py```.
5) Import the file ```data.csv``` into stata (see ```readin.do``` as reference).


